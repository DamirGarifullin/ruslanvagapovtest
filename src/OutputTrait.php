<?php

trait OutputTrait
{
    /**
     * @param string $message
     */
    protected function output($message)
    {
        echo $message, "\n";
    }
}
