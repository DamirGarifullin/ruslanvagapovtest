<?php

class Doctor
{
    /**
     * @var string
     */
    private $specialization;

    /**
     * @var array
     */
    private $config;

    /**
     * @param string $specialization
     * @param array $config
     */
    public function __construct($specialization, $config)
    {
        $this->specialization = $specialization;
        $this->config = $config;
    }

    /**
     * @return string
     */
    public function getSpecialization()
    {
        return $this->specialization;
    }

    /**
     * Приём больного
     *
     * @param Patient $patient
     */
    public function conductReception(\Patient $patient)
    {
        if ($this->config['isGivesReference'] && $patient->isNeedReference()) {
            $patient->needReference(false);
        }

        if ($this->config['isGivesDirection'] && $patient->isNeedDirection()) {
            $patient->needDirection(false);
        }

        foreach ($this->config['illness'] as $illness) {
            // в 20% случаев сразу вылечить не удаётся
            if (mt_rand(1, 5) != 1) {
                $patient->removeIllness($illness);
            }
        }

        sleep(1);
    }
}
