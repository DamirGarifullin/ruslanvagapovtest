<?php

class Patient
{
    /**
     * @var array
     */
    private $illnesses = [];

    /**
     * @var bool Нужно направление
     */
    private $isNeedDirection = false;

    /**
     * @var bool Нужна справка
     */
    private $isNeedReference = false;

    /**
     * @var string
     */
    private $name;

    /**
     * @var bool Здоров?
     */
    private $isHealthy = true;

    /**
     * @var array
     */
    private $config;

    /**
     * @param string $name
     * @param array $config
     */
    public function __construct($name, $config)
    {
        $this->name = $name;
        $this->config = $config;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name . ': '
            . ($this->isNeedReference ? 'need reference, ' : '')
            . ($this->isNeedDirection ? 'need direction, ' : '')
            . '[' . implode(', ', $this->illnesses) . ']';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $illness
     */
    public function addIllness($illness)
    {
        if (!in_array($illness, $this->illnesses)) {
            $this->illnesses[] = $illness;
        }

        $this->isHealthy = false;
    }

    /**
     * @param string $illness
     */
    public function removeIllness($illness)
    {
        $key = array_search($illness, $this->illnesses);

        if ($key !== false) {
            unset($this->illnesses[$key]);
        }
    }

    /**
     * @param bool $needDirection
     */
    public function needDirection($needDirection = true)
    {
        $this->isNeedDirection = $needDirection;
    }

    /**
     * @return bool
     */
    public function isNeedDirection()
    {
        return $this->isNeedDirection;
    }

    /**
     * @param bool $needReference
     */
    public function needReference($needReference = true)
    {
        $this->isNeedReference = $needReference;
    }

    /**
     * @return bool
     */
    public function isNeedReference()
    {
        return $this->isNeedReference;
    }

    /**
     * @param Doctor $doctor
     */
    public function goToReception(\Doctor $doctor)
    {
        if ($this->checkDoctorSuitable($doctor)) {
            $doctor->conductReception($this);
        }
    }

    /**
     * Проверка, что доктор нам подходит
     *
     * @param Doctor $doctor
     * @return bool
     */
    protected function checkDoctorSuitable(\Doctor $doctor)
    {
        if (isset($this->config[$doctor->getSpecialization()])) {
            $doctorCapability = $this->config[$doctor->getSpecialization()];

            if ($this->isNeedReference && $doctorCapability['isGivesReference']) {
                return true;
            }

            if ($this->isNeedDirection && $doctorCapability['isGivesDirection']) {
                return true;
            }

            if (!empty(array_intersect($this->illnesses, $doctorCapability['illness']))) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isHealthy()
    {
        return !$this->isNeedDirection && !$this->isNeedReference && empty($this->illnesses);
    }
}
