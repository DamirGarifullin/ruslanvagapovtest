<?php

class Clinic
{
    use OutputTrait;

    /**
     * @var \Patient[]
     */
    protected $patientsQueue = [];

    /**
     * @var \Doctor[]
     */
    protected $doctors = [];

    /**
     * @param \Doctor[] $doctors
     * @param \Patient[] $patients
     */
    public function __construct($doctors, $patients)
    {
        $this->doctors = $doctors;
        $this->patientsQueue = $patients;
    }

    public function run()
    {
        $doctorsIter = new \InfiniteIterator(new \ArrayIterator($this->doctors));

        $this->output('start of the reception');

        foreach ($doctorsIter as $doctor) {
            /** @var \Doctor $doctor */

            $this->output('Doctor ' . $doctor->getSpecialization());

            // за смену доктор принимает столько пациентов
            $receptionCount = 5;

            // когда начинается новый приём очередь опрашивается с начала
            foreach ($this->patientsQueue as $idx => $patient) {
                /** @var \Patient $patient */

                $this->output((string)$patient);

                $patient->goToReception($doctor);

                // выгоняем выздоровевшего из очереди
                if ($patient->isHealthy()) {
                    $this->output($patient->getName() . ' is completely healthy!');

                    unset($this->patientsQueue[$idx]);
                }

                if (--$receptionCount <= 0) {
                    // приём этим доктором окончен
                    break;
                }
            }

            if (!count($this->patientsQueue)) {
                break;
            }
        }

        $this->output('end of the reception');
    }
}
