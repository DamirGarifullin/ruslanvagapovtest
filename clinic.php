#!/usr/bin/php
<?php

set_time_limit(0);

$projectDir = __DIR__ . '/src/';

spl_autoload_register(function ($className) use ($projectDir) {
    $filePath = $projectDir . $className . '.php';

    if (file_exists($filePath)) {
        require_once $filePath;
    }
});

$docConfigFile = __DIR__ . '/doctors.json';
$docConfig = json_decode(file_get_contents($docConfigFile), true);

$doctors = [];
$illness = [];

foreach ($docConfig as $docSpecialization => $conf) {
    $doctors[] = new \Doctor($docSpecialization, $conf);
    $illness = array_merge($illness, $conf['illness']);
}

$patientsQueue = [];

for ($i = 1; $i <= 20; $i++) {
    $patient = new \Patient('Patient ' . $i, $docConfig);

    // каждому 4-му нужно напр-ие
    if ($i % 4 == 0) {
        $patient->needDirection();
    }

    // каждому 5-му нужна справка
    if ($i % 5 == 0) {
        $patient->needReference();
    }

    // выбираем случайные болячки и награждаем ими пациента
    $illnessKeys = (array)array_rand($illness, mt_rand(1, count($illness)));
    foreach ($illnessKeys as $key) {
        $patient->addIllness($illness[$key]);
    }

    $patientsQueue[] = $patient;
}

(new \Clinic($doctors, $patientsQueue))->run();
